#!/bin/bash

readconf() {
	local topic_expr="^[[:space:]]*\[[[:space:]]*(\"(.*)\"|([^;]*?))[[:space:]]*\][[:space:]]*(;.+)?$"
	local value_expr="^[[:space:]]*(;.*|(\"(.+?)\"|([[:alnum:]_-]+))[[:space:]]*=[[:space:]]*(\"(.*)\"|([^;]*)))?[[:space:]]*(;.+)?$"
	local i current file="$1" topic="${3:+$2}" param="${3:-$2}"
	while read -r line; do ((i++))
		if [[ "$line" =~ $topic_expr ]]; then current="${BASH_REMATCH[2]:-${BASH_REMATCH[3]}}"
		elif [[ "$line" =~ $value_expr ]]; then
			if [[ "$topic" == "$current" && "$param" == "${BASH_REMATCH[3]:-${BASH_REMATCH[4]}}" ]]
			then echo "${BASH_REMATCH[6]:-${BASH_REMATCH[7]}}" | sed -e 's/\s*$//' && return 0; fi
		else >&2 echo "($file:$i) Malformed configuration: $line" && return 1; fi
	done < "$file"
	return ${#param}
}


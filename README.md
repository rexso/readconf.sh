# readconf.sh
Simple Bash function to read configuration files

## Example:

Script using **readconf**:
```bash
#!/bin/bash

# Configuration file name
conf="path/to/some.conf"

# Load readconf function
source "readconf.sh"

# Validate configuration file format
if ! readconf "$conf"; then exit; fi

# If the configuration file is malformed, an error message is printed to stderr.

foostr=$(readconf "$conf" "string" "foo") # Read 'foo' value from 'string' topic
barstr=$(readconf "$conf" "string" "bar") # Read 'bar' value from 'string' topic

foonum=$(readconf "$conf" "number" "foo") # Read 'foo' value from 'number' topic
barnum=$(readconf "$conf" "number" "bar") # Read 'bar' value from 'number' topic
```

Example **configuration** file:
```
; Empty lines and lines starting with a semicolon are considered valid.
; Spaces and horisontal tabs are ignored unless encapsulated in quotes.

; Both keys and values can contain semicolons and any amount of spaces
; and horisontal tabs if encapuslated in quotation characters.

; Topics are optional and are defined by encapsulating a string inside
; square brackets. Also topic names can be encapsulated with quotation
; characters to support names including semicolons or square brackets.


[string]
foo = unquoted string value     ; text after semicolons are comments
bar = "string with ; character" ; quoted strings can contain semicolons

[number]
foo =                           ; values can be empty strings
bar = 1337
```
#!/bin/bash

# Configuration file
conf="demo.conf"

# Load readconf function
source ../readconf.sh

# Validate configuration file format
if ! readconf "$conf"; then exit; fi

# Read some configuration values

echo foo: $(readconf "$conf" "foo")
echo bar: $(readconf "$conf" "bar")
echo baz: $(readconf "$conf" "baz")

echo string foo: $(readconf "$conf" "strings" "foo")
echo string bar: $(readconf "$conf" "strings" "bar")
echo string baz: $(readconf "$conf" "strings" "baz")

echo number foo: $(readconf "$conf" "numbers" "foo")
echo number bar: $(readconf "$conf" "numbers" "bar")
echo number baz: $(readconf "$conf" "numbers" "baz")

